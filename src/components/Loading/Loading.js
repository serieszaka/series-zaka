import React from 'react';
import { Icon } from 'antd';

import './Loading.scss';


export default function Loading() {
    return(
        <div className = "loading">
           
            <Icon type="loading" />
        </div>
    )
}
