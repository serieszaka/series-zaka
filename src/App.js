import React from 'react';
import {Layout} from "antd"
import  {BrowserRouter as Router,Switch ,Route} from 'react-router-dom';
import MenuTop from './components/MenuTop';
// import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
// import { Provider } from 'react-redux';  //, useSelector,
// import { createStore, applyMiddleware } from 'redux';
// import { save, load } from "redux-localstorage-simple";

//Paginas//
import Home from './pages/Home'
import NewMovies from './pages/new-movies'
import Popular from './pages/popular'
import Search from './pages/search/search'
import Movie from './pages/movie/'
import Error404 from './pages/error404' 
import Actores from './pages/Actores'
import Registro from './pages/registro/Registro'


//      /                         /__\                        \
//    /      Luke! We're         |><> |                         \
//  /       going to have        (/\__)                           \
// |           company!         /      \                           |
// |                            ||/(===o                           |
// |                            | /  \ |                           |
// |                            \/][][\/                           |
// |                             |\  /|                            |
// |                             |_||_|                            |
// |                             [ ][ ]                            |
//  \                            | || |                           /
//   \                           | || |                         /
//    \  ________________________[_][_]_______________________/



//Aqui es en donde comienza toda la magia//
export default function App() {
  const {Header,Content} = Layout;
  return (
    
   <div>
     <Layout>
      <Router>
        <Header style={{zIndex: 1 }}>
        <MenuTop />
        </Header>
        <Content>
          <Switch>
          <Route path="/" exact={true}>
            <Home/>
          </Route>
          <Route path="/new-movies" exact={true}>
            <NewMovies/>
          </Route>
          <Route path="/popular" exact={true}>
            <Popular/>
          </Route>
          <Route path="/registro" exact={true}>
            <Registro/>
          </Route>
          <Route path="/search" exact={true}>
            <Search/>
          </Route>

          <Route path="/movie/:id" exact={true}>
            <Movie/>
          </Route>
          <Route path="/actores" exact={true}>
            <Actores/>
          </Route>
          <Route path="*">
            <Error404/>
          </Route>
         
         
        </Switch>
        </Content>
      </Router>
     </Layout>
     
    
   </div>
   
   
  );
}


